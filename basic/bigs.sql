-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Nov 2022 pada 07.52
-- Versi server: 10.4.21-MariaDB
-- Versi PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bigs`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_datapasien`
--

CREATE TABLE `master_datapasien` (
  `id` int(11) NOT NULL,
  `no_rekam_medis` varchar(250) NOT NULL,
  `nama` varchar(250) NOT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') NOT NULL,
  `tanggal_lahir` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_datapasien`
--

INSERT INTO `master_datapasien` (`id`, `no_rekam_medis`, `nama`, `jenis_kelamin`, `tanggal_lahir`) VALUES
(1, '00-00-00-01', 'Tina Martha', 'perempuan', '1990-02-03'),
(2, '00-00-00-02', 'Radinal Dwiki Novendra', 'laki-laki', '1995-09-10'),
(3, '00-00-00-03', 'Desy Arjuna', 'perempuan', '1993-12-16'),
(4, '00-00-00-04', 'Alfy Syahri', 'laki-laki', '1995-01-01'),
(5, '00-00-00-05', 'Nanang Sunardi', 'laki-laki', '1989-02-04'),
(6, '00-00-00-06', 'Suzana', 'perempuan', '1991-01-02'),
(7, '00-00-00-07', 'Riko', 'laki-laki', '1989-08-09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `trx_registrasi`
--

CREATE TABLE `trx_registrasi` (
  `id` int(11) NOT NULL,
  `waktu_registrasi` datetime NOT NULL,
  `no_registrasi` varchar(250) NOT NULL,
  `id_pasien` int(11) NOT NULL,
  `jenis_registrasi` varchar(250) NOT NULL,
  `layanan` varchar(250) NOT NULL,
  `jenis_pembayaran` varchar(250) NOT NULL,
  `status_registrasi` enum('Aktif','Tutup Kunjungan') NOT NULL,
  `waktu_mulai_pelayanan` datetime NOT NULL,
  `waktu_selesai_pelayanan` datetime NOT NULL,
  `petugas_pendaftaran` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `trx_registrasi`
--

INSERT INTO `trx_registrasi` (`id`, `waktu_registrasi`, `no_registrasi`, `id_pasien`, `jenis_registrasi`, `layanan`, `jenis_pembayaran`, `status_registrasi`, `waktu_mulai_pelayanan`, `waktu_selesai_pelayanan`, `petugas_pendaftaran`) VALUES
(1, '2022-11-11 14:44:08', '2001060001', 1, 'Rawat Jalan', 'Poliklinik Umum', 'Umum', 'Tutup Kunjungan', '2022-11-10 14:44:08', '2022-11-13 06:10:51', 'Safitri Jayanti'),
(7, '2022-11-13 06:04:55', '2001060002', 2, 'Rawat Jalan', 'Poliklinik Gigi', 'Umum', 'Aktif', '2022-11-13 06:04:55', '0000-00-00 00:00:00', 'Ahmad Sandi'),
(8, '2022-11-13 06:06:53', '2001060003', 3, 'Rawat Jalan', 'Poliklinik Obgyn', 'BPJS Kesehatan', 'Tutup Kunjungan', '2022-11-13 06:06:53', '2022-11-13 06:18:49', 'Cici Utami'),
(9, '2022-11-13 06:08:30', '2001060004', 4, 'UGD', 'UGD', 'Mandiri Inhealth', 'Tutup Kunjungan', '2022-11-13 06:08:30', '2022-11-13 06:08:43', 'Putri Aisha'),
(10, '2022-11-13 06:10:12', '2001060005', 4, 'Rawat Inap', 'Kelas 1', 'Mandiri Inhealth', 'Aktif', '2022-11-13 06:10:12', '0000-00-00 00:00:00', 'Putri Aisha'),
(11, '2022-11-13 06:12:39', '2001070001', 5, 'Rawat Jalan', 'Poliklinik Mata', 'BNI Life', 'Tutup Kunjungan', '2022-11-13 06:12:39', '2022-11-13 06:19:10', 'Safitri Jayanti'),
(12, '2022-11-13 06:13:56', '2001070002', 6, 'UDG', 'UDG', 'BPJS Kesehatan', 'Tutup Kunjungan', '2022-11-13 06:13:56', '2022-11-13 06:14:10', 'Safitri Jayanti'),
(13, '2022-11-13 06:15:36', '2001070003', 6, 'Rawat Inap', 'Kelas', 'BPJS Kesehatan', 'Aktif', '2022-11-13 06:15:36', '0000-00-00 00:00:00', 'Safitri Jayanti'),
(14, '2022-11-13 06:17:11', '2001070004', 7, 'Rawat Jalan', 'Poliklinik Umum', 'Umum', 'Aktif', '2022-11-13 06:17:11', '0000-00-00 00:00:00', 'Ahmad Sandi'),
(15, '2022-11-13 06:18:22', '2001070005', 2, 'Rawat Jalan', 'Poliklinik Gigi', 'Umum', 'Aktif', '2022-11-13 06:18:22', '0000-00-00 00:00:00', 'Ahmad Sandi');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `master_datapasien`
--
ALTER TABLE `master_datapasien`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `trx_registrasi`
--
ALTER TABLE `trx_registrasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pasien` (`id_pasien`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `master_datapasien`
--
ALTER TABLE `master_datapasien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `trx_registrasi`
--
ALTER TABLE `trx_registrasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `trx_registrasi`
--
ALTER TABLE `trx_registrasi`
  ADD CONSTRAINT `pasien` FOREIGN KEY (`id_pasien`) REFERENCES `master_datapasien` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
